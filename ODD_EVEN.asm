.MODEL SMALL
.STACK 100h
.DATA
    MGS DB "ENTER A NUMBER: $" 
    EVEN1 DB "IT'S AN EVEN NUMBER.$"
    ODD1 DB "IT'S AN ODD NUMBER.$"     

.CODE
    MAIN PROC 
        MOV AX, @DATA   ; INITIALIZE DATA SEGMENT
        MOV DS,AX
        ;----------------------------------------   
     AGAIN:
        ;SHOW MGS ...............................
        LEA DX,MGS      ; LOAD THE STRING TO DX FOR PRINT
        MOV AH,9        ; STRING PRINT KORTE HOBE
        INT 21H   
        ;----------------------------------------   
        
        ;LOP TO TAKE MULTIDIGIT NUMBER...........  
        
        MOV AH,1   ; TO TAKE INPUT A CHAR
        INT 21H 
        MOV BL,AL  
        
        CMP AL,0DH  ; IS INPUR enter OR NOT!
        JE END      ; IF enter JUMP TO END
        
        WHILE:
            INT 21H  
            
            CMP AL,0DH        ; IS INPUR enter OR NOT!
            JE WHILE_END      ; IF enter JUMP TO END 
            MOV BL,AL 
            JMP WHILE
            
        ;ENDING LOOP------------------------------
        
        WHILE_END: 
        ;NEW LINE---------------------------------
            MOV AH,2       
            MOV DL,0DH
            INT 21H
            MOV DL,0AH
            INT 21H
        ;NEW LINE END-----------------------------    
           
           TEST BL,1  ; IS EVEN??
           JZ EVEN    ; IF EVEN JUMP TO EVEN
           JMP ODD    ; ELSE JUMP TO ODD 
           JMP END  
           ;.....................................
           
        EVEN:
            LEA DX,EVEN1      ; SHOW MGS IF EVEN
            MOV AH,9
            INT 21H  
            JMP END   
            ;......................................
            
        ODD:
            LEA DX,ODD1      ; SHOW MGS IF ODD
            MOV AH,9
            INT 21H  
            JMP END   
        
          
    
        END: 
            ;NEW LINE---------------------------------
            MOV AH,2       
            MOV DL,0DH
            INT 21H
            MOV DL,0AH
            INT 21H
            ;NEW LINE END----------------------------- 
             JMP AGAIN
     
        ;..................RETURN.................
	    MOV AH,4CH      ; RETURN 0
	    int 21h    
        
    MAIN ENDP



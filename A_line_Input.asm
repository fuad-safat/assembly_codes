.MODEL SMALL
.STACK 100H
.DATA
    ;VARAIABALES WILL BE DECLARED 
    SAFAT DB "TYPE A LINE: $"   
    
    MGS1 DB "FIRST CAPITAL: $"
    MGS2 DB "LAST CAPITAL: $" 
     FIRST DB 0
     LAST DB 0
    
.CODE
    MAIN PROC
        MOV AX, @DATA    ;INITIALIZE DATA SEGMENT
        MOV DS, AX
        ;--------------------------------------------------- 
        
        LEA DX, SAFAT    ;LOAD THE STRING TO DX FOR PRINT
        MOV AH,9         ;FOR PRINTING   
        INT 21H  
        
            
      ;INPUT A CHAR------------------------------------------
      MOV AH,1
      INT 21H  
      
      MOV FIRST,'Z'       ;initilize first and last 
      MOV LAST,'A'
      
      ;LOOP START..........................................
      WHILE_:             ; loop
            CMP AL,0DH
            JE END        ; je = jump if equal
            INC DX  
            CMP AL,'Z'    ;compare is AL == z or not
            JLE CHECK     ;jle = jump if less then or equal
            RE:
            
            INT 21H  
            
            
            JMP WHILE_
 
      
      END_WHILE:  
      ;LOOP END.............................................
      
      CHECK:   
           CMP AL,'A'          ;CHECKING IS CHAR GRATER THEN OR EQUAL A OR NOT?
           JGE MAKE_DICISION1  ; IF YES THEN JIMP TO  MAKE_DICISION1
           
           JMP RE 
      ;CHECKING END----------------------------------------  
           
      
      MAKE_DICISION1:
           CMP AL,FIRST 
           JL MAKE_FIRST 
           
           JMP MAKE_DICISION2
               
           
      MAKE_FIRST:
           MOV FIRST,AL 
           JMP MAKE_DICISION2
           JMP RE 
           
           
      MAKE_DICISION2:
           CMP AL,LAST
           JG MAKE_LAST  
           
           jmp re  
           
      MAKE_LAST:
          MOV LAST,AL 
          JMP RE   
          
          
            
    END:  
    ;.....................................................
        MOV AH,2       ; NEW LINE
        MOV DL,0DH
        INT 21H
        MOV DL,0AH
        INT 21H   
    ;......................................................   
                
        
        LEA DX, MGS1    ;LOAD THE STRING TO DX FOR PRINT
        MOV AH,9        ;FOR PRINTING   
        INT 21H  
        
        
        ;PRINT FIRST CAPITAL
        MOV AH,2
        MOV DL,FIRST
        INT 21H
        
    ;......................................................
                 
        MOV AH,2          ;NEW LINE
        MOV DL,0DH
        INT 21H
        MOV DL,0AH
        INT 21H  
        ;--------------------------------------------------
        
        LEA DX, MGS2     ;LOAD THE STRING TO DX FOR PRINT
        MOV AH,9         ;FOR PRINTING   
        INT 21H 
        
        
        ;PRINT LAST CAPITAL
        MOV AH,2
        MOV DL,LAST
        INT 21H
        
        
        
        MOV AH,4CH        ;WORKS AS RETURN 0
        INT 21H 
        
    MAIN ENDP
    ;OTHER PROCEDURES(IF ANY)
    
    
    
    
    
    
    
    
    ;; TODAY ONLINE
    .MODEL SMALL
.STACK 100H
.DATA
    ;VARAIABALES WILL BE DECLARED 
    SAFAT DB "Enter a line: $"   
    
    MGS1 DB "tHERE IS SMALL LETTER: $"
    MGS2 DB "NO SMALL LETTERS : $" 
     FIRST DB 0
     ;LAST DB 0
    
.CODE
    MAIN PROC
        MOV AX, @DATA    ;INITIALIZE DATA SEGMENT
        MOV DS, AX
        ;--------------------------------------------------- 
         
        MOV CX,3
        F: 
          MOV AH,2       
        MOV DL,0DH
        INT 21H
        MOV DL,0AH
        INT 21H   
        
        LEA DX, SAFAT    
        MOV AH,9           
        INT 21H 
        
         
        
        
        MOV AH,1
      INT 21H  
      
      MOV FIRST,'z'       
   
      MOV BL,0
    
      WHILE_:             
            CMP AL,0DH
            JE TAKE_DES        
            INC DX  
            CMP AL,'z'   
            JLE CHECK    
            RE:
            
            INT 21H  
            
            
            JMP WHILE_
 
      
      END_WHILE: 
      
       
      
      
      
      CHECK:   
           CMP AL,'a'          
           JGE MAKE_DICISION1  
           
           JMP RE 
   
     
           
      
      MAKE_DICISION1:
           CMP AL,FIRST 
           JL MAKE_FIRST 
           JMP RE
           
       
               
           
      MAKE_FIRST:
           MOV FIRST,AL
            
           MOV BL, 1
           JMP RE 
           
           
    
    TAKE_DES:
        CMP BL,0
        JE  LAST
        
     
    
          
            
    END:  
    ;.....................................................
        MOV AH,2       
        MOV DL,0DH
        INT 21H
        MOV DL,0AH
        INT 21H   
    ;......................................................   
                
        
        LEA DX, MGS1   
        MOV AH,9       
        INT 21H  
        
        
       
        MOV AH,2
        MOV DL,FIRST
        INT 21H  
        
        LOOP F  
        
        
        
    ;......................................................
       
     LAST: 
     
       MOV AH,2       
        MOV DL,0DH
        INT 21H
        MOV DL,0AH
        INT 21H   
        
        LEA DX, MGS2   
        MOV AH,9       
        INT 21H 
        LOOP F 
        
       
        
      E:  
     
        MOV AH,4CH        ;WORKS AS RETURN 0
        INT 21H 
        
    MAIN ENDP
   